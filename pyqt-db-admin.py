# -*- coding: utf-8 -*-

import sys
from PyQt5 import QtWidgets

import pyqt_dba.main_window

if __name__ == '__main__':


    app = QtWidgets.QApplication( sys.argv )

    splashScreen = pyqt_dba.main_window.MainWindow.show_splash()
    app.processEvents()

    window = pyqt_dba.main_window.MainWindow()

    splashScreen.finish( window )
    window.show()

    retVal = sys.exit( app.exec_() )
    # TODO deal with retVal eg 999=restart

