
pyqt-db-admin
=========================================

An interface to view database

.. image:: _static/screenshot.png
    :width: 600px

Contents:

.. toctree::
   :maxdepth: 2

   api/index.rst
   todo



Git Source
-------------


clone the git repository

.. code-block::

    git clone https://gitlab.com/pyqtx/pyqt-db-admin.git



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

