img.*
==========================================================

.. automodule:: img
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

