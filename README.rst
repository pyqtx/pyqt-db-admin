pyqt-db-admin
====================

This is dedicated to Larry's cats and kittens :-)

A desktop version of phpMyAdmin, sqlfrontEnd + others, but smarter..

- https://pyqtx.gitlab.io/pyqt-db-admin/

.. image:: https://pyqtx.gitlab.io/pyqt-db-admin/_images/screenshot.png



