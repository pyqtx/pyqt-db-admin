# -*- coding: utf-8 -*-

import sys
import os

from Qt import Qt, QtGui, QtCore, QtWidgets



from . import APP_NAME

import G
from img import Ico
import ut
import xwidgets

from servers import server_dialog, servers_list
from db_widgets import db_panel



class MainWindow(QtWidgets.QMainWindow):
    """Main application Window"""

    def on_after(self):
        """Executes a few moments after show()"""
        #self.on_server_new()
        #self.on_servers_list()

        servers = G.settings.value("servers", [])
        for srv in servers:
            if srv['auto_connect']:
                self.on_open_db(srv)

        return


    def on_open_db(self, params):
        widget = db_panel.DbPanelWidget(params=params)
        self.add_widget(widget, params['name'], Ico.db)

    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self)


        QtWidgets.QApplication.setOrganizationName("pyqtx")
        QtWidgets.QApplication.setOrganizationDomain("pyqtx.gitlab.io")
        QtWidgets.QApplication.setApplicationName(APP_NAME)
        QtWidgets.QApplication.setApplicationVersion("1.0.1") # changing this looses previous settings

        self.outlook_items = []

        self.setWindowTitle('PyQt Db Admin')
        self.setWindowIcon(Ico.logo())



        ##==========================================
        #  Menus


        ##= Main file/dba
        self.menuDba = self.menuBar().addMenu("DBA")

        self.menuDba.addSeparator()


        self.actQuit = self.menuDba.addAction( "Quit", self.on_quit)
        self.actQuit.setIconVisibleInMenu(True)


        ##= Servers Menu ==
        self.menuServers = self.menuBar().addMenu("Servers")

        widgetAction = QtWidgets.QWidgetAction(self.menuServers)
        self.serversListWidget = servers_list.ServersListWidget()
        self.serversListWidget.sigConnect.connect(self.on_open_db)
        widgetAction.setDefaultWidget(self.serversListWidget)
        self.menuServers.addAction(widgetAction)

        self.menuServers.addSeparator()

        ## Settings Menu ==
        self.menuSettings = self.menuBar().addMenu("Settings")

        self.menuSettings.addSeparator()

        ## ====
        # set Style Menu
        self.menuStyle = self.menuSettings.addMenu("Style")
        self.menuStyle.triggered.connect(self.on_style)
        actGroup = QtWidgets.QActionGroup(self)
        for i in QtWidgets.QStyleFactory.keys():
            act = self.menuStyle.addAction(i)
            act.setCheckable(True)
            if QtWidgets.QApplication.style().objectName() == i:
                act.setChecked(True)
            actGroup.addAction(act)



        ## Help Menu
        self.menuHelp = self.menuBar().addMenu("Help")

        self.menuHelp.addSeparator()
        self.menuHelp.addAction("About %s" % APP_NAME, self.on_about)
        self.menuHelp.addSeparator()
        self.menuHelp.addAction("About Qt", self.on_about_qt)

        ## ========================
        ## Docks
        # self.serversListWidget = servers_list.ServersListWidget()
        # dockServers = ut.make_dock("Servers", self.serversListWidget)
        # self.addDockWidget(Qt.LeftDockWidgetArea, dockServers)



        ## ========================
        ## Main Central <>
        self.mainWidget = QtWidgets.QWidget()
        self.setCentralWidget(self.mainWidget)

        self.mainLayout = xwidgets.vlayout()
        self.mainWidget.setLayout(self.mainLayout)

        # stick tabs in layout so can put space at end
        tabLay = xwidgets.hlayout()
        self.mainLayout.addLayout(tabLay)

        self.tabBar = QtWidgets.QTabBar()
        tabLay.addWidget(self.tabBar)

        self.tabBar.setExpanding(True)
        self.tabBar.setTabsClosable( True )
        self.tabBar.setMovable(False)
        self.tabBar.setUsesScrollButtons(True)

        self.tabBar.tabCloseRequested.connect(self.on_tab_close_requested)
        self.tabBar.currentChanged.connect(self.on_tab_changed)

        tabLay.addStretch(10) # tabs forced left and space end

        # =======================
        ## Widget Deck
        m = 0
        self.appDeck = QtWidgets.QStackedLayout()
        self.appDeck.setContentsMargins( m, m, m, m )
        self.mainLayout.addLayout( self.appDeck, 20 )


        if True:
            # if G.settings.is_zeb():
            self.setGeometry( 10, 10, 1200, 800 )
            self.move( 1600, 20 )
            self.setWindowState( QtCore.Qt.WindowMaximized )

        else:
            # G.settings.restore_window( "DevPrevWindow", self )
            self.setWindowState( QtCore.Qt.WindowMaximized )


        QtCore.QTimer.singleShot(200, self.on_after)

    # =========================================================================
    # Helpers
    # =========================================================================
    def msg( self, message, pos=None, timeout=2000, warn=False ):
        self.msgWidget.show_message( message, pos=pos, warn=warn )


    def closeEvent( self, event ):
        G.settings.save_window( self )


    ##=========================================================================================================
    def on_quit( self ):
        ret = QtWidgets.QMessageBox.warning( self, APP_NAME, "Sure you want to Quit ?", QtGui.QMessageBox.No | QtGui.QMessageBox.Yes )
        if ret == QtWidgets.QMessageBox.Yes:
            G.settings.save_window(  self )
            sys.exit( 0 )




    ########################
    def on_style( self, action ):
        QtWidgets.QApplication.setStyle( QtWidgets.QStyleFactory.create( action.text() ) )



    ##===================================
    def on_close_tab(self):
        self.flash("Sorry - Close Tab is TODO", info=True)
        pass

    def on_close_other_tabs(self):
        self.flash("Sorry - Close Other Tab is TODO", info=True)
        pass

    def on_close_all_tabs(self):
        self.flash("Sorry - Close All Tab is TODO", info=True)
        pass


    def close_tab(self, wname, xxid=None):

        idx, w = self.get_widget(wname, xxid)
        if idx:
            self.on_app_tab_close_requested(idx)


    def on_tab_close_requested( self, idx ):
        widget = self.appDeck.widget(idx)
        self.appDeck.removeWidget(widget)
        self.tabBar.blockSignals(True)
        self.tabBar.removeTab(idx)
        self.tabBar.blockSignals(False)

    def on_tab_changed(self, idx):
        self.appDeck.setCurrentIndex(idx)

    def on_open_web_site( self, action ):
        url = QtCore.QUrl( action.property( "url" ).toString() )
        G.ut.open_url( url )

    def on_about( self ):
        QtWidgets.QMessageBox.about( self, "Contact Pedro", "pyqtx@daffodil.uk.com" )

    def on_about_qt( self ):
        QtWidgets.QMessageBox.aboutQt( self )




    # ====================================================================
    def add_widget(self, widget, label, ico, closable=True):

        # idx = self.appDeck.addWidget(w)
        idx = self.appDeck.count()
        self.appDeck.insertWidget(idx, widget)
        self.appDeck.setCurrentIndex(idx)

        self.tabBar.blockSignals(True)

        # else:

        self.tabBar.addTab( Ico.icon(ico), "%s" % label )
        self.tabBar.setCurrentIndex(idx)
        #self.tabBar.setTabData( idx, widget )
        self.tabBar.blockSignals(False)
        return idx

    @staticmethod
    def show_splash():

        splashImage = QtGui.QPixmap("../images/splash.png")
        splashScreen = QtWidgets.QSplashScreen(splashImage)
        splashScreen.showMessage("  Loading ..........." )
        splashScreen.show()
        return splashScreen

