# -*- coding: utf-8 -*-

import os

"""
Application Globals

"""

HERE_PATH =  os.path.abspath(os.path.dirname(__file__))
ROOT_PATH = os.path.abspath(os.path.join(HERE_PATH, ".."))


from pyqtx import xsettings


settings = xsettings.XSettings()
"""Instance of :class:`pyqtx.xsettings.XSettings` """



