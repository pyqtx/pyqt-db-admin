# -*- coding: utf-8 -*-

# see https://github.com/spyder-ide/qtawesome


import os

from Qt import QtGui, QtCore, QtWidgets



import qtawesome as qta


class Ico:
    """Icons Helper


      - All icons used are listed as class constants.
      - This is a base set using `QtAwesome <https://github.com/spyder-ide/qtawesome>`_.
      - One can override this class as a base set and add custom maybe..

    """

    favicon = ["fa5s.database", "orange"]
    caret_down = "fa5s.angle-down"
    help = "fa5s.question-circle"
    node = "fa5s.angle-right"

    machine = "fa5s.angle-right"

    commander = "fa5s.solar-panel"
    terminal = "fa5s.desktop"
    return_key = "ei.return-key"
    connect = "fa5s.sign-in-alt"

    add = "fa5s.plus"
    edit = "fa5.edit"
    delete = "fa5.trash-alt"

    cancel = "ei.remove"
    save = "ei.ok-circle"

    settings = "fa5s.cog"
    refresh = "ei.refresh"
    login = "ei.lock"

    start = "fa5s.play"
    stop = "fa5s.stop"

    up = "ei.arrow-up"
    down = "ei.arrow-down"

    quit = "ei.eject"
    clear = "ei.remove-circle"

    filter_on = "fa5s.server"
    filter_off = "fa5s.server"

    server = ["fa5s.server", "purple"]
    servers = ["ei.th-list", "yellow"]

    ##== db
    db = "fa5s.database"
    db_tables = ["ei.th-list", "lightblue"]

    db_table = ["fa5s.table", "lightblue"]
    db_view = ["fa5s.table", "orange"]

    db_rows = ["ei.th-list", "yellow"]
    db_columns = ["fa5s.columns", "purple"]
    db_column = ["fa5s.square", "purple"]


    #db_data = ["ei.th-list", "yellow"]


    ## Engines
    alembic = ["fa5s.wrench", "darkred"]
    spatial = ["ei.map-marker", "darkblue"]


    @staticmethod
    def i(name):
        """Shortcut to :meth:`Ico.icon` """
        return Ico.icon(name)

    @staticmethod
    def icon( obj, color="#666666" ):
        """Creates a QIcon

        :param name: font icon to load
        :type name: str
        :return: QIcon
        :rtype: QIcon
        """
        if isinstance(obj, list):
            ico_name = obj[0]
            color = obj[1]
        else:
            ico_name = obj


        return qta.icon(ico_name, color=color)

    @staticmethod
    def logo():
        """Override this to custom logo"""
        return Ico.icon(Ico.favicon)

    @staticmethod
    def table(t):
        if t == "VIEW":
            return Ico.icon(Ico.db_view)
        elif t == "BASE TABLE":
            return Ico.icon(Ico.db_table)
        return Ico.icon(Ico.favicon)