
import os
import sys

APP_NAME = "pyqt-db-admin"


DBA_ROOT = os.path.abspath(os.path.dirname(__file__))

if not DBA_ROOT in sys.path:
    sys.path.insert(0, DBA_ROOT)




class Table:

    def __init__(self, tbl_dict):

        self.tbl_dict = tbl_dict
        self.schema_col = None
        self.name_col = None

    @property
    def name(self):
        return self.tbl_dict['table_name']
