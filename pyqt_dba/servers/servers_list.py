# -*- coding: utf-8 -*-

from Qt import QtCore, QtWidgets, Qt, pyqtSignal

import xwidgets
from img import Ico
import G

from servers import server_dialog, servers_model

class ServersListWidget(QtWidgets.QWidget):

    sigConnect = pyqtSignal(dict)

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        self.model = servers_model.ServersModel()


        self.mainLayout = xwidgets.vlayout()
        self.setLayout(self.mainLayout)


        self.toolbar = QtWidgets.QToolBar()
        self.toolbar.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        self.mainLayout.addWidget(self.toolbar)

        # Creds

        self.actionAdd = xwidgets.XToolButton(text="Add", ico=Ico.add, clicked=self.on_add)
        self.toolbar.addWidget(self.actionAdd)

        self.actionEdit = xwidgets.XToolButton(text="Edit", ico=Ico.edit, clicked=self.on_edit)
        self.toolbar.addWidget(self.actionEdit)



        self.actionDelete = xwidgets.XToolButton(text="Delete", ico=Ico.delete, clicked=self.on_delete)
        self.toolbar.addWidget(self.actionDelete)


        self.toolbar.addSeparator()
        self.actionConnect = xwidgets.XToolButton(text="Connect", ico=Ico.connect, clicked=self.on_connect)
        self.toolbar.addWidget(self.actionConnect)


        self.tree = QtWidgets.QTreeView()
        self.mainLayout.addWidget(self.tree)

        self.tree.setModel(self.model)
        self.tree.doubleClicked.connect(self.on_tree_double)
        self.tree.selectionModel().selectionChanged.connect(self.on_connect)


        self.load()



    def load(self):
        self.model.load()
        self.on_tree_selection()

    def on_tree_double(self):
        self.on_edit()

    def on_tree_selection(self, sel=None, desel=None):
        ena = self.get_rec() is not None
        self.actionDelete.setEnabled(ena)
        self.actionEdit.setEnabled(ena)
        self.actionConnect.setEnabled(ena)


    def get_rec(self):
        ridx = self.get_ridx()
        if ridx is None:
            return
        return self.model.recs[ridx]

    def get_ridx(self):
        sm = self.tree.selectionModel()
        if not sm.hasSelection():
            return None
        return sm.selectedIndexes()[0].row()

    def on_add(self):
        self.show_edit_dialog(None)

    def on_edit(self):
        idx = self.get_ridx()
        self.show_edit_dialog(idx)

    def show_edit_dialog(self, idx):
        dial = server_dialog.ServerDialog(self, idx=idx)
        if dial.exec_():
            self.load()

    def on_delete(self):
        sm = self.tree.selectionModel()
        if not sm.hasSelection():
            return

        resp = QtWidgets.QMessageBox.question(self, "Delete ?", "delete server")
        if resp == QtWidgets.QMessageBox.Yes:
            rec = self.model.recs[sm.selectedIndexes()[0].row()]
            #G.settings.remove_server(rec)
            self.load()


    def on_connect(self):
        rec = self.get_rec()
        self.sigConnect.emit(rec)

