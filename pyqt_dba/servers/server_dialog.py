

from Qt import QtCore, QtWidgets, QtSql, Qt, pyqtSignal
import xwidgets

import G
from img import Ico



class ServerDialog(QtWidgets.QDialog):

    sigSaved = pyqtSignal(dict)


    def __init__(self, parent=None, idx=None):
        super().__init__(parent)

        self.idx = idx

        self.setWindowTitle("Server Details")
        self.setWindowIcon(Ico.icon(Ico.server))
        self.setMinimumWidth(400)


        self.mainLayout = xwidgets.vlayout(margin=20, spacing=10)
        self.setLayout(self.mainLayout)


        if self.idx == None:
            s = "Create a new server connection"
            #self.lbl = xwidgets.InfoLabel(s, style=xwidgets.InfoLabel.INFO)
            #self.mainLayout.addWidget(self.lbl)

        self.grid = QtWidgets.QGridLayout()
        self.grid.setSpacing(10)
        self.mainLayout.addLayout(self.grid)

        row = 0
        self.grid.addWidget(QtWidgets.QLabel("Label / Name"), row, 0, Qt.AlignRight)
        self.txtServerName = xwidgets.XLineEdit(changed=self.update_url)
        self.grid.addWidget(self.txtServerName, row, 1, 1, 2)
        self.txtServerName.setPlaceholderText("eg My Server")

        row  += 1
        self.grid.addWidget(QtWidgets.QLabel("Engine"), row, 0, Qt.AlignRight)
        self.cmbEngine = xwidgets.XComboBox(self)
        self.grid.addWidget(self.cmbEngine, row, 1, 1, 2)


        row  += 1
        self.grid.addWidget(QtWidgets.QLabel("Address"), row, 0, Qt.AlignRight)
        self.txtServerAddress = xwidgets.XLineEdit(changed=self.update_url)
        self.txtServerAddress.setPlaceholderText("127.0.0.1:1234")
        self.grid.addWidget(self.txtServerAddress, row, 1, 1, 2)
        self.txtServerAddress.textChanged.connect(self.on_validate)

        row += 1
        self.grid.addWidget(QtWidgets.QLabel("Port"), row, 0, Qt.AlignRight)
        self.spinPort = xwidgets.XSpinBox(changed=self.update_url)
        self.grid.addWidget(self.spinPort, row, 1)
        self.spinPort.setRange(0, 70000)

        row += 1
        self.grid.addWidget(QtWidgets.QLabel("User"), row, 0, Qt.AlignRight)
        self.txtUser = xwidgets.XLineEdit(changed=self.update_url)
        self.grid.addWidget(self.txtUser, row, 1)

        row += 1
        self.grid.addWidget(QtWidgets.QLabel("Password"), row, 0, Qt.AlignRight)
        self.txtPass = xwidgets.XLineEdit(changed=self.update_url)
        self.grid.addWidget(self.txtPass, row, 1)

        row += 1
        self.grid.addWidget(QtWidgets.QLabel("Db Name"), row, 0, Qt.AlignRight)
        self.txtDb = xwidgets.XLineEdit(changed=self.update_url)
        self.grid.addWidget(self.txtDb, row, 1)

        row += 1

        self.buttTestLogin = xwidgets.XToolButton(self, text="Test Login",
                                                  autoRaise=False, clicked=self.on_test_login)
        self.grid.addWidget(self.buttTestLogin, row, 1, Qt.AlignRight)


        row += 1
        self.chkAutoConnect = QtWidgets.QCheckBox()
        self.chkAutoConnect.setText("Auto connect at startup")
        self.grid.addWidget(self.chkAutoConnect, row, 1)


        row += 1
        self.txtUrl = xwidgets.XLineEdit()
        #self.txtUrl.setText()
        self.grid.addWidget(self.txtUrl, row, 1)


        row += 1
        self.txtMess = QtWidgets.QLabel("")
        self.grid.addWidget(self.txtMess, row, 1)

        self.grid.setColumnStretch(0, 1)
        self.grid.setColumnStretch(1, 1)
        self.grid.setColumnStretch(2, 1)

        self.statusBar = xwidgets.StatusBar(self, refresh=False)
        self.mainLayout.addWidget(self.statusBar)

        self.formAction = xwidgets.FormActionBar(self)
        self.mainLayout.addWidget(self.formAction)

        self.load()



    def load(self):

        self.cmbEngine.addItem("-- select --")
        for eng in QtSql.QSqlDatabase.drivers():
            self.cmbEngine.addItem(eng)

        if self.idx != None:
            srv = G.settings.value("servers")[self.idx]

            idx = self.cmbEngine.findText(srv['engine'])
            if idx != -1:
                self.cmbEngine.setCurrentIndex(idx)

            self.txtServerName.setText(srv['name'])
            self.txtServerAddress.setText(srv['address'])
            self.spinPort.setValue(srv['port'])
            self.txtUser.setText(srv['user'])
            self.txtPass.setText(srv['password'])
            self.txtDb.setText(srv['database'])
            self.chkAutoConnect.setChecked(srv['auto_connect'])


    def on_cancel(self):
        self.reject()

    def on_validate(self):

        s = self.txtServerAddress.s()
        url = QtCore.QUrl(s)
        ena = False

    def update_url(self):

        url = QtCore.QUrl()
        url.setHost(self.txtServerAddress.text())
        url.setUserName(self.txtUser.text())
        url.setPassword(self.txtPass.text())
        url.setPath(self.txtDb.text())
        print(url.toString())
        self.txtUrl.setText(url.toString())



    def on_save(self):

        for w in self.txtServerName, self.txtServerAddress, self.txtUser, self.txtPass:
            if len(w.s()) < 4:
                w.setFocus()
                return

        srv = dict(
            name = self.txtServerName.s(),
            engine = self.cmbEngine.s(),
            address = self.txtServerAddress.s(),
            port = self.spinPort.value(),
            user = self.txtUser.s(),
            password = self.txtPass.s(),
            database =self.txtDb.s(),
            auto_connect = self.chkAutoConnect.isChecked()
        )
        servers = G.settings.value("servers", [])
        if self.idx == None:
            servers.append(srv)
        else:
            servers[self.idx] = srv
        G.settings.setValue("servers", servers)
        print(srv)

        self.accept()




    def on_ping(self):
        addr = self.txtServerAddress.s()
        server = server_conn.ServerConn(self, server_address=addr)
        server.get(self, "/ping", tag="ping")

    def on_test_login(self):

        data = dict(login=self.txtUser.s(), password=self.txtPass.s())

        addr = self.txtServerAddress.s()
        server = server_conn.ServerConn(self, server_address=addr)
        server.post(self, "/login", tag="login", data=data)

    def set_dirty(self, foo=None, bar=None, foobar=None):
        self.formAction.set_dirty()