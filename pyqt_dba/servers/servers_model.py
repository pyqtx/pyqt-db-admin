# -*- coding: utf-8 -*-

from Qt import QtCore, QtWidgets, Qt, pyqtSignal

from img import Ico
import G

class C:
    autoconnect = 0
    name = 1
    address = 2
    port = 3
    user = 4
    password = 5
    database = 6


    _count = 7


class ServersModel(QtCore.QAbstractTableModel):

    sigLoaded = pyqtSignal()

    def data(self, midx, role):

        rec = self.recs[midx.row()]
        cidx = midx.column()

        if role == Qt.DisplayRole:
            if cidx == C.name:
                return rec["name"]

            elif cidx == C.address:
                return rec['address']

            elif cidx == C.port:
                return rec['port']

            elif cidx == C.address:
                return rec['address']

            elif cidx == C.user:
                return rec['user']

            elif cidx == C.password:
                return rec['password']

            elif cidx == C.database:
                return rec['database']

            elif cidx == C.autoconnect:
                return "Yes" if rec['auto_connect'] else "-"

        if role == Qt.DecorationRole and  cidx == C.name:
            return Ico.icon(Ico.server)



    def __init__(self, parent=None):
        super(QtCore.QAbstractTableModel, self).__init__()

        self.recs = []


    def columnCount(self, pidx=None):
        return C._count

    def rowCount(self, pidx=None):
        return len(self.recs)

    def headerData(self, idx, orientation, role):
        if role == Qt.DisplayRole:
            return ["OnStart", "Name", "Address", "Port", "User", "Pass", "Database"][idx]


    def load(self):

        self.recs = G.settings.value("servers", [])
        self.modelReset.emit()