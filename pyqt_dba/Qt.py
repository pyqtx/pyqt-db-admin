# -*- coding: utf-8 -*-

"""The idea is to later inject different engines here,
idea from pyqtChart project

```
# import PySide.QtWidgets as QtWidgets
# import PyQt5.QtWidgets as QtWidgets
```

"""


## Essentials
from PyQt5 import QtCore
from PyQt5.QtCore import Qt

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import pyqtSlot


from PyQt5 import QtGui
from PyQt5 import QtWidgets

from PyQt5 import QtNetwork

from PyQt5 import QtWebEngineWidgets
from PyQt5 import QtWebChannel


#from PyQt5 import QtWebSockets
#from PyQt5 import QtSql


try:
    from PyQt5 import QtSql
except:
    pass
