

from Qt import QtCore, QtGui, QtWidgets, Qt, pyqtSignal

import xwidgets
from img import Ico



class DataViewWidget(QtWidgets.QWidget):

    sigSaved = pyqtSignal(dict)


    def __init__(self, parent=None, db=None, table=None):
        super().__init__(parent)

        assert db != None
        assert table != None
        self.db = db
        self.table = table

        self.model = TableDataModel(db=self.db, table=table)


        self.mainLayout = xwidgets.vlayout()
        self.setLayout(self.mainLayout)

        self.toolbar = xwidgets.XToolBar()
        self.mainLayout.addWidget(self.toolbar)

        self.buttRefresh = xwidgets.XToolButton(self, ico=Ico.refresh, text="Refresh", clicked=self.model.load)
        self.toolbar.addWidget(self.buttRefresh)

        self.buttRefresh = xwidgets.XToolButton(self, ico=Ico.delete, text="Truncate", clicked=self.on_truncate_table)
        self.toolbar.addWidget(self.buttRefresh)



        self.tree = QtWidgets.QTreeView()
        self.tree.setRootIsDecorated(False)
        self.mainLayout.addWidget(self.tree)
        self.tree.doubleClicked.connect(self.on_tree_double)

        self.tree.setModel(self.model)

        self.model.sigLoaded.connect(self.on_model_loaded)
        self.model.load()


    def on_tree_double(self):
        pass

    def on_model_loaded(self, c):
        self.tree.setUpdatesEnabled(False)

        for c in range(0, self.model.rowCount()):
            self.tree.resizeColumnToContents(c)
            if self.tree.columnWidth(c) > 150:
                self.tree.setColumnWidth(c, 150)

        self.tree.setUpdatesEnabled(True)

    def on_truncate_table(self):

        db_query.truncate_table(self.db, self.table)


class TableDataModel(QtCore.QAbstractTableModel):

    sigLoaded = pyqtSignal(int)

    def __init__(self, db=None, table=None):
        super(QtCore.QAbstractTableModel, self).__init__()

        assert db != None
        assert table != None
        self.db = db
        self.table = table

        self.rows = []
        self.cols = []


    def load(self):

        self.cols, self.rows, err = self.db.table_data(self.table)
        print(self.rows, self.cols, err)
        self.modelReset.emit()
        self.sigLoaded.emit(len(self.rows))

    def columnCount(self, pidx=None):
        return len(self.cols)

    def rowCount(self, pidx=None):
        return len(self.rows)

    def headerData(self, idx, orientation, role):
        if role == Qt.DisplayRole:
            return self.cols[idx]

    def data(self, midx, role):
        if role == Qt.DisplayRole:
            return str(self.rows[midx.row()][self.cols[midx.column()]])


