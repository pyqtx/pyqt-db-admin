
import sqlalchemy as sa

from Qt import QtCore, QtWidgets, Qt, pyqtSignal

import G
from img import Ico
import xwidgets

from dba_con import psycop_query



from . import tables_list, table_view


class DbPanelWidget(QtWidgets.QMainWindow):

    sigConnected = pyqtSignal()
    sigError = pyqtSignal(object)


    def __init__(self, parent=None, params=None):
        super().__init__(parent)

        assert params != None
        self.params = params

        print("params=", params, self)

        self.db = psycop_query.PsyCopConn()
        self.db.url = "postgresql+psycopg2://carma:carma@127.0.0.1:5433/carma_dev"


        if False:
            self.db_url = "postgresql+psycopg2://carma:carma@127.0.0.1:5433/carma_dev"
            self.engine = sa.create_engine(self.db_url)
            self.inspector = sa.inspect(self.engine)
            schemas = self.inspector.get_schema_names()
            for schema in schemas:
                print("schema: %s" % schema)
                for table_name in self.inspector.get_table_names(schema=schema):
                    print("Table: %s" % table_name)
                    for column in self.inspector.get_columns(table_name, schema=schema):
                        #print("Column: %s" % column)
                        pass



        self.topToolBar = xwidgets.XToolBar()
        self.addToolBar(Qt.TopToolBarArea, self.topToolBar)

        self.tbgDbInfo = xwidgets.ToolBarGroup(self, title="Connection")
        self.topToolBar.addWidget(self.tbgDbInfo)

        self.lblDbConn = xwidgets.XLabel()
        self.tbgDbInfo.addWidget(self.lblDbConn)

        #self.statusBar = xwidgets.StatusBar(self)
        #self.topToolBar.addWidget(self.statusBar)


        ## Main Hybryd tab/stack widget for closable etc
        self.tabStack = xwidgets.XTabStack()
        self.setCentralWidget(self.tabStack)


        ## Tables List and Preview in a splitter
        self.mainSplitter = xwidgets.XSplitter()
        self.tabStack.addTab(self.mainSplitter, ico=Ico.db_tables, text="Tables", closable=False)

        self.tablesList = tables_list.TablesListWidget(db=self.db)
        self.tablesList.sigTable.connect(self.on_table_open)

        self.mainSplitter.addWidget(self.tablesList)
        #self.tabStack.setTabIcon(nidx, Ico.i(Ico.db_tables))


        # Connect a few moments later
        QtCore.QTimer.singleShot(400, self.db_connect)



    def on_table_open(self, table):

        # find if this table is already open
        for idx, widget in enumerate(self.tabStack.widgets()):
            if isinstance(widget, table_view.TableViewWidget):
                if swidget.table['table_name'] == table['table_name']:
                    sself.tabStack.setCurrentIndex(idx)

        widget = table_view.TableViewWidget(db=self.db, table=table)
        nidx = self.tabStack.addTab(widget, text=table['table_name'],
                                    ico=Ico.db_table)
        #self.tabWidget.setTabIcon(nidx, Ico.i(Ico.db_table))
        self.tabStack.setCurrentIndex(nidx)

    def db_connect(self):

        self.db.connect()
        self.tablesList.load()

        # QSsqlDataBase
        return
        self.db.setHostName("%s" % self.params['address'])

        self.db.setDatabaseName(self.params['database'])
        self.db.setUserName(self.params['user'])
        self.db.setPassword(self.params['password'])

        ok = self.db.open()
        if ok:
            self.sigConnected.emit()
            return
        err = self.db.lastError()
        self.sigError.emit(err.text())
        self.statusBar.showMessage(err.text(), warn=True)



    def on_tab_close_requested(self, idx):
        self.tabWidget.removeTab(idx)
