

from Qt import QtCore, QtGui, QtWidgets, Qt, pyqtSignal
import xwidgets

from img import Ico



class ColumnsViewWidget(QtWidgets.QWidget):


    def __init__(self, parent=None, db=None, table=None):
        super().__init__(parent)

        assert db != None
        assert table != None
        self.db = db
        self.table = table

        self.model = ColumnsModel(db=self.db, table=table)


        self.mainLayout = xwidgets.vlayout()
        self.setLayout(self.mainLayout)

        self.tree = QtWidgets.QTreeView()
        self.tree.setRootIsDecorated(False)
        self.mainLayout.addWidget(self.tree)
        self.tree.doubleClicked.connect(self.on_tree_double)

        self.tree.setModel(self.model)

        self.model.load()


    def on_tree_double(self):
        pass


class ColumnsModel(QtCore.QAbstractTableModel):

    sigLoaded = pyqtSignal(int)

    def __init__(self, db=None, table=None):
        super().__init__()

        assert db != None
        assert table != None
        self.db = db
        self.table = table

        self.rows = []
        self.cols = []


    def load(self):

        self.cols, self.rows, err = self.db.columns_info(self.table['table_name'])

        self.modelReset.emit()
        self.sigLoaded.emit(len(self.rows))

    def columnCount(self, pidx=None):
        return len(self.cols)

    def rowCount(self, pidx=None):
        return len(self.rows)

    def headerData(self, idx, orientation, role):
        if role == Qt.DisplayRole:
            return self.cols[idx]

    def data(self, midx, role):
        cname = self.cols[midx.column()]
        if role == Qt.DisplayRole:
            v = self.rows[midx.row()][cname]
            if v == None:
                return ""
            return v
        #print(cname)
        if cname == "column_name":
            if role == Qt.DecorationRole:
                return Ico.i(Ico.db_column)

            if role == Qt.FontRole:
                f = QtGui.QFont()
                f.setBold(True)
                return f


