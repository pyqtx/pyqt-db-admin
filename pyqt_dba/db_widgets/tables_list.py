

from Qt import QtCore, QtWidgets, QtGui, Qt, pyqtSignal

import G
from img import Ico
import xwidgets



class TablesListWidget(QtWidgets.QWidget):

    sigTable = pyqtSignal(dict)


    def __init__(self, parent=None, db=None):
        super().__init__(parent)

        assert db != None

        self.model = TablesListModel(db=db)
        self.model.sigLoaded.connect(self.on_model_loaded)

        self.proxyModel = TablesListProxyModel()
        self.proxyModel.setSourceModel(self.model)

        self.mainLayout = xwidgets.vlayout()
        self.setLayout(self.mainLayout)

        # == Toolbar
        self.toolbar = xwidgets.XToolBar()
        self.mainLayout.addWidget(self.toolbar)

        self.buttGroupFilter = xwidgets.XButtonGroup(self, exclusive=False)

        self.tbgSchema = xwidgets.ToolBarGroup(title="Schema", is_group=True,
                                               exclusive=False,
                                               toggle_callback=self.update_proxy_model)
        self.toolbar.addWidget(self.tbgSchema)

        self.tbgTypes = xwidgets.ToolBarGroup(title="Table Type", is_group=True,
                                              exclusive=False,
                                              toggle_callback=self.update_proxy_model)
        self.toolbar.addWidget(self.tbgTypes)

        self.tbgShowHide = xwidgets.ToolBarGroup(title="Cols", is_group=False)
        self.toolbar.addWidget(self.tbgShowHide)

        self.buttHideCols = xwidgets.XToolButton(text="Hide Cols", checkable=True, checked=True,
                                                 clicked=self.toggle_hidden_cols)
        self.tbgShowHide.addWidget(self.buttHideCols)

        self.toolbar.addStretch()

        self.tbgActions = xwidgets.ToolBarGroup(title="Actions")
        self.toolbar.addWidget(self.tbgActions)

        self.buttRefresh = xwidgets.XToolButton(self, ico=Ico.refresh, both=False, clicked=self.model.load)
        self.tbgActions.addWidget(self.buttRefresh)


        #== Tree
        self.tree = QtWidgets.QTreeView()
        self.mainLayout.addWidget(self.tree)

        self.tree.setModel(self.proxyModel)
        self.tree.setRootIsDecorated(False)
        self.tree.setSortingEnabled(True)

        self.tree.doubleClicked.connect(self.on_tree_double)



    def toggle_hidden_cols(self):
        state = self.buttHideCols.isChecked()
        for c in range(0, self.model.columnCount()):
            if not c in [ self.model.table_name_col, self.model.row_count_col]:
                self.tree.setColumnHidden(c, state)

    def on_model_loaded(self):
        if self.model.initial_load:
            return


        self.load_schemas_toolbar()
        self.load_table_types_toolbar()
        self.update_proxy_model()

        for c in range(0, self.model.columnCount()):
            self.tree.resizeColumnToContents(c)
        self.tree.sortByColumn(self.model.table_name_col, Qt.AscendingOrder)
        self.toggle_hidden_cols()

    def on_tree_double(self, pidx):
        sidx = self.proxyModel.mapToSource(pidx)
        rec = self.model.rows[sidx.row()]
        self.sigTable.emit(rec)

    def load_table_types_toolbar(self):
        for idx, typ in enumerate(self.model.types):
            icon = Ico.table(typ)
            self.tbgTypes.addButton(text=typ, icon=icon, checkable=True, checked=typ=="BASE TABLE")
        self.update_proxy_model()

    def load_schemas_toolbar(self):
        for idx, sch in enumerate(self.model.schemas):

            self.tbgSchema.addButton(text=sch, checkable=True, checked=sch=="public")
        self.update_proxy_model()

    def update_proxy_model(self, foo=None, bar=None):

        del self.proxyModel.schemas[:]
        for butt in self.tbgSchema.buttonGroup.buttons():
            if butt.isChecked():
                self.proxyModel.schemas.append(butt.text())

        del self.proxyModel.table_types[:]
        for butt in self.tbgTypes.buttonGroup.buttons():
            if butt.isChecked():
                self.proxyModel.table_types.append(butt.text())
        self.proxyModel.invalidate()

    def set_db(self, db):
        self.model.db = db

    def load(self):
        self.model.load()


class TablesListProxyModel(QtCore.QSortFilterProxyModel):
    def __init__(self):
        super().__init__()

        self.table_types = []
        self.schemas = []

    def filterAcceptsRow(self, ridx, parent):
        rec = self.sourceModel().rows[ridx]
        if rec["table_type"] in self.table_types and rec["table_schema"] in self.schemas:
            return True

        return False


class TablesListModel(QtCore.QAbstractTableModel):

    sigLoaded = pyqtSignal(int)


    def __init__(self, db=None):
        super().__init__()

        assert db != None
        self.db = db

        self.initial_load = False
        self.table_schema_col = None
        self.table_name_col = None
        self.rows = []
        self.cols = []
        self.schemas = []
        self.types = []


    def load(self):

        self.cols, self.rows, err =  self.db.tables()
        #print(self.cols)
        #print(self.rows)
        if err:
            print("err=", err.text())
            return

        #self.table_schema_col = self.cols.index("table_schema")
        idx = self.cols.index("table_name")
        self.cols.insert(idx, "__row_count__")
        self.table_name_col = self.cols.index("table_name")
        self.row_count_col = self.cols.index("__row_count__")

        ## get the unique table_type
        types = []
        schemas = []
        for rec in self.rows:
            cc, err = self.db.row_count("%s.%s" % (rec['table_schema'],rec['table_name']))
            rec["__row_count__"] = cc
            if not rec["table_schema"] in schemas:
                schemas.append(rec["table_schema"])

            if not rec["table_type"] in types:
                types.append(rec["table_type"])

        self.types = sorted(types)
        self.schemas = sorted(schemas)

        self.modelReset.emit()
        self.sigLoaded.emit(len(self.rows))


    def columnCount(self, pidx=None):
        return len(self.cols)

    def rowCount(self, pidx=None):
        return len(self.rows)

    def headerData(self, idx, orientation, role):
        if role == Qt.DisplayRole:
            if idx == self.row_count_col:
                return "rows"
            return self.cols[idx]

    def data(self, midx, role):

        if role == Qt.DisplayRole:
            return self.rows[midx.row()][self.cols[midx.column()]]

        if midx.column() == self.table_name_col:
            rec = self.rows[midx.row()]
            if role == Qt.DecorationRole:
                tname = rec['table_name']
                if tname == "alembic_version":
                    return Ico.i(Ico.alembic)
                elif tname == "spatial_ref_sys":
                    return Ico.i(Ico.spatial)

                typ = rec["table_type"]
                return Ico.table(typ)

            if role == Qt.FontRole:
                f = QtGui.QFont()
                f.setBold(True)
                return f

        elif midx.column() ==self.row_count_col:

           if role == Qt.TextAlignmentRole:
               return Qt.AlignCenter
