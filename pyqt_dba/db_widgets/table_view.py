

from Qt import QtCore, QtGui, QtWidgets, Qt, pyqtSignal
import xwidgets

import G
from img import Ico

from db_widgets import table_cols_view, table_data_view

class TableViewWidget(QtWidgets.QWidget):

    sigTable = pyqtSignal(dict)


    def __init__(self, parent=None, db=None, table=None):
        super().__init__(parent)

        assert db != None
        assert table != None
        self.db = db
        self.table = table

        self.mainLayout = xwidgets.vlayout()
        self.setLayout(self.mainLayout)

        self.tabWidget = xwidgets.XTabWidget()
        self.mainLayout.addWidget(self.tabWidget)

        self.tableData = table_data_view.DataViewWidget(db=self.db, table=self.table)
        self.tabWidget.addTab(self.tableData, ico=Ico.db_rows, text="Data")

        self.tableCols = table_cols_view.ColumnsViewWidget(db=self.db, table=self.table)
        self.tabWidget.addTab(self.tableCols, text="Columns", ico=Ico.db_columns)

        self.tabWidget.setCurrentIndex(1)


