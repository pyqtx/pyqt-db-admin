
from Qt import QtSql


def query_select(db: QtSql.QSqlDatabase, sql: str):
    query = QtSql.QSqlQuery(sql, db)
    print("SQL=", sql)
    #query.
    if not query.isActive():

        print(query.lastError().text())
        return None, None, query.lastError()
        pass

    record = query.record()
    col_count = record.count()
    cols = []
    for idx in range(0, col_count):
        col_name = record.fieldName(idx)
        cols.append(col_name.lower())

    rows = []
    while query.next():
        # if query.value(1) in ["pg_catalog", "information_schema"]:
        #    continue

        rec = []
        for idx in range(0, col_count):
            rec.append(query.value(idx))
        rows.append(rec)
    print(cols)
    return rows, cols, None

def tables(db):

    sql = "select * from information_schema.tables"
    return query_select(db, sql)

def truncate_table(db: QtSql.QSqlDatabase, table: dict)-> tuple:

    sql = 'truncate table %s' % table['table_name']


def table_data(db: QtSql.QSqlDatabase, table: dict) -> tuple:

    sql = "select * from %s" % table['table_name']

    return query_select(db, sql)
    return
    query = QtSql.QSqlQuery(sql, db)

    record = query.record()
    col_count = record.count()

    cols = []
    for idx in range(0, col_count):
        col_name = record.fieldName(idx)
        cols.append(col_name)

    recs = []
    while query.next():
        rec = []
        for idx in range(0, col_count):
            rec.append(query.value(idx))
        recs.append(rec)