
from Qt import QtCore
import psycopg2
import psycopg2.extras

class PsyCopConn():

    def __init__(self):

        self.url = None
        self.conn = None
        self.db = None


    def connect(self):
        url = QtCore.QUrl(self.url)

        self.conn = psycopg2.connect(host=url.host(),
                                     port=url.port(),
                                     user=url.userName(),
                                     password=url.password(),
                                     dbname=url.path()[1:],
                                     )
        self.cursor = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)


    def query_select(self, sql):
        #query = QtSql.QSqlQuery(sql, db)
        print("SQL=", sql)
        self.cursor.execute(sql)
        #for desc in self.cursor.description:
        #    print(desc)

        return [desc[0] for desc in self.cursor.description], \
               self.cursor.fetchall(), \
               None


    def tables(self):
        """Returns list of tables"""
        sql = "select * from information_schema.tables"
        return self.query_select(sql)

    def row_count(self, tbl):
        sql = "select count(*) as c from %s " % tbl
        #print(sql)
        self.cursor.execute(sql)
        return self.cursor.fetchone()['c'], None

    def columns_info(self, table_name):
        sql = "select * from INFORMATION_SCHEMA.columns "
        sql += "where TABLE_NAME = '%s' " % table_name
        return self.query_select(sql)

    def truncate_table(self, db , table):

        sql = 'truncate table %s' % table['table_name']


    def table_data(self,table):

        sql = "select * from %s" % table['table_name']

        return self.query_select(sql)
